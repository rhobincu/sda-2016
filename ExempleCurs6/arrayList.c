#include "arrayList.h"
#include <stdlib.h>
#include <string.h>

struct ArrayList * createArrayList(unsigned initialCapacity) {
    struct ArrayList * newList = (struct ArrayList *) malloc(sizeof (struct ArrayList));

    if (newList == NULL) {
        return NULL;
    }

    newList->size = 0;
    newList->capacity = initialCapacity;
    newList->array = (short *) malloc(initialCapacity * sizeof (short));

    return newList;
}

void deleteArrayList(struct ArrayList * list) {
    free(list->array);
    free(list);
}

void mergeSort(short * array, unsigned start /*inclusiv*/, unsigned end /* exclusisv*/) {

    if (end - start == 1) return;

    unsigned middle = (start + end) / 2;
    mergeSort(array, start, middle);
    mergeSort(array, middle, end);

    short tmpArray[end - start];

    unsigned i = start; // indexul in prima jumatate
    unsigned j = middle; // indexul in a doua jumatate
    unsigned k = 0; //index in tmpArray

    while (i != middle && j != end) {
        if (array[i] <= array[j]) {
            tmpArray[k++] = array[i++];
        } else {
            tmpArray[k++] = array[j++];
        }
    }

    if (i != middle) {
        memcpy(tmpArray + k, array + i, (middle - i) * sizeof (short));
    } else if (j != end) {
        memcpy(tmpArray + k, array + j, (end - j) * sizeof (short));
    }

    memcpy(array + start, tmpArray, (end - start) * sizeof (short));
}

void arrayListMergeSort(struct ArrayList * list) {
    mergeSort(list->array, 0 /*inclusiv*/, list->size /* exclusiv*/);
}

void quickSort(short * array, unsigned start /*inclusiv*/, unsigned end /* inclusiv*/) {
    if (start == end) return;
    
    unsigned pivot = start;
    unsigned index = end;
    int direction = -1;
    
    while(pivot != index){
        if((direction == -1 && array[pivot] > array[index]) ||
                (direction == 1 && array[index] > array[pivot])){
            short tmpValue = array[pivot];
            array[pivot] = array[index];
            array[index] = tmpValue;
            
            unsigned tmpIndex = index;
            index = pivot;
            pivot = tmpIndex;
            
            direction = -direction;
        }
        
        index += direction;
    }
    
    if(pivot != start) quickSort(array, start, pivot - 1);
    if(pivot != end) quickSort(array, pivot + 1, end);
}

void arrayListQuickSort(struct ArrayList * list) {
    quickSort(list->array, 0 /*inclusiv*/, list->size - 1 /*inclusiv*/);
}
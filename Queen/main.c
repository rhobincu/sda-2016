/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: rhobincu
 *
 * Created on May 20, 2016, 11:39 AM
 */

#include <stdio.h>
#include <stdlib.h>

void init(int * stack, int stackHead) {
    stack[stackHead] = -1;
}

char succesor(int * stack, int stackHead, int stackSize) {
    if (stack[stackHead] == stackSize - 1) return 0;

    stack[stackHead]++;
    return 1;
}

char isSolution(int * stack, int stackHead, int stackSize) {
    return stackHead == stackSize - 1;
}

void printSolution(int * stack, int stackSize) {
    int i;
    for (i = 0; i < stackSize; i++) printf("%d ", stack[i]);

    printf("\n");
}

char validate(int * stack, int stackHead) {

    int i;
    for (i = 0; i < stackHead; i++) {

        /* verify not on same column*/
        if (stack[i] == stack[stackHead]) return 0;

        /* verify not on the same diagonal*/
        if (abs(stack[i] - stack[stackHead]) == abs(i - stackHead)) return 0;
    }

    return 1;
}

void doQueen(int n) {
    int stackHead = 0;
    int stack[n];
    char isValid, hasSuccesor;
    init(stack, stackHead);
    while (stackHead >= 0) {
        do {
            hasSuccesor = succesor(stack, stackHead, n);
            isValid = 0;
            if (hasSuccesor) isValid = validate(stack, stackHead);
        } while (hasSuccesor && !isValid);

        if (isValid) {
            if (isSolution(stack, stackHead, n)) {
                //printSolution(stack, n);
            } else {
                init(stack, ++stackHead);
            }
        } else {
            stackHead--;
        }
    }
}

/*
 * 
 */
int main(int argc, char** argv) {

    int n;
    printf("n = ");
    //scanf("%d", &n);
    n = 20;
    doQueen(n);

    return (EXIT_SUCCESS);
}

